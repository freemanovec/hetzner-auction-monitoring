import std.stdio;
import std.net.curl;
import std.datetime;
import std.conv : to;

import hetzner.hetzner_listing;

void main()
{
	SysTime current_time = Clock.currTime();
	immutable ulong current_timestamp = current_time.toUnixTime();
	immutable string current_timestamp_string = to!string(current_timestamp);
	string listing_url = "https://www.hetzner.com/a_hz_serverboerse/live_data.json?m=" ~ current_timestamp_string;
	writeln(listing_url);
	char[] response_data_bytes = get(listing_url);
	string response_json = to!string(response_data_bytes);
	HetznerListing listing = new HetznerListing(response_json);
	foreach(ref s; listing.servers){
		writeln(s.type, " for ", s.price, "€, perf. ", s.cpu_benchmark);
	}
	writeln(response_data_bytes.length);
}
