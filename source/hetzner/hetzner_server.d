module hetzner.hetzner_server;

import std.json : JSONValue;
import std.string : strip;
import std.conv : to;

class HetznerServer {

    string
        type,
        freetext,
        cpu,
        datacenter;
    string[]
        description;
    uint
        cpu_benchmark,
        bandwidth,
        ram,
        drive_size;
    byte
        cpu_count,
        drive_count;
    bool
        high_io,
        ecc,
        traffic_capped,
        fixed_price;
    double
        price;

    this(
        string _type,
        string[] _description,
        string _freetext,
        uint _cpu_benchmark,
        byte _cpu_count,
        bool _high_io,
        bool _ecc,
        bool _traffic_capped,
        uint _bandwidth,
        uint _ram,
        double _price,
        uint _drive_size,
        byte _drive_count,
        bool _fixed_price,
        string _datacenter,
        string _cpu
    ){
        type = _type;
        description = _description;
        freetext = _freetext;
        cpu_benchmark = _cpu_benchmark;
        cpu_count = _cpu_count;
        high_io = _high_io;
        ecc = _ecc;
        traffic_capped = _traffic_capped;
        bandwidth = _bandwidth;
        ram = _ram;
        price = _price;
        drive_size = _drive_size;
        drive_count = _drive_count;
        fixed_price = _fixed_price;
        datacenter = _datacenter;
        cpu = _cpu;
    }

    this(JSONValue _base){
        immutable string type = strip(_base["name"].str);
        immutable string freetext = strip(_base["freetext"].str);
        JSONValue[] description_objects = _base["description"].array;
        string[] description = new string[description_objects.length];
        for(uint i = 0; i < description.length; i++){
            description[i] = strip(description_objects[i].str);
        }
        immutable string cpu = strip(_base["cpu"].str);
        immutable uint cpu_benchmark = to!uint(_base["cpu_benchmark"].integer);
        immutable uint bandwidth = to!uint(_base["bandwith"].integer);
        immutable uint ram = to!uint(_base["ram"].integer);
        immutable uint drive_size = to!uint(_base["hdd_size"].integer);
        immutable byte cpu_count = to!byte(_base["cpu_count"].integer);
        immutable byte drive_count = to!byte(_base["hdd_count"].integer);
        immutable bool high_io = _base["is_highio"].boolean;
        immutable bool ecc = _base["is_ecc"].boolean;
        immutable bool traffic_capped = _base["traffic"].str != "unlimited";
        immutable bool fixed_price = _base["fixed_price"].boolean;
        immutable string datacenter = _base["datacenter"].array[0].str;
        immutable double price = to!double(_base["price"].str);

        this(
            type,
            description,
            freetext,
            cpu_benchmark,
            cpu_count,
            high_io,
            ecc,
            traffic_capped,
            bandwidth,
            ram,
            price,
            drive_size,
            drive_count,
            fixed_price,
            datacenter,
            cpu
        );
    }

}