module hetzner.hetzner_listing;

import std.json : parseJSON, JSONValue;
import std.stdio : writeln;
import std.conv : to;
import std.string : strip;
import hetzner.hetzner_server;

class HetznerListing {

    HetznerServer[] servers;

    this(
        HetznerServer[] _servers
    ){
        servers = _servers;
    }

    this(JSONValue _base){
        JSONValue servers_json = _base["server"];
        JSONValue[] servers_values = servers_json.array;
        HetznerServer[] _servers = new HetznerServer[servers_values.length];
        for(uint i = 0; i < _servers.length; i++){
            _servers[i] = new HetznerServer(servers_json[i]);
        }

        this(_servers);
    }

    this(
        string _listingJson
    ){
        JSONValue json = parseJSON(_listingJson);

        this(json);
    }

}